#!/bin/bash

echo -e "\e[1;34mInstalling some dependencies...\e[0;39m"

sudo apt install -y build-essential cmake unzip pkg-config git python3-dev python3-pip \
libjpeg-dev libpng-dev libtiff-dev \
libavcodec-dev libavformat-dev libswscale-dev \
libv4l-dev libxvidcore-dev libx264-dev \
libgtk-3-dev \
libatlas-base-dev gfortran \
libxml2-dev libaudit-dev libusb-1.0-0-dev gettext libgirepository1.0-dev \
libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libnotify-dev \
gtk-doc-tools


echo -e "\e[1;34mMaking sure that meson and ninja are installed\e[0;39m"

sudo apt update && sudo apt upgrade -y && sudo apt install meson -y

echo -e "\e[1;34mMeson version\e[0;39m"
meson --version

echo -e "\e[1;34mNinja version\e[0;39m"
ninja --version

DIR="~/SETUP_NVIDIA"
if [ -d "$DIR" ]; then
  cd ~/SETUP_NVIDIA
else
  mkdir ~/SETUP_NVIDIA && cd ~/SETUP_NVIDIA
fi

echo -e "\e[1;34mDownloading Aravis 0.6 (for OpenCV backend)\e[0;39m"

wget https://github.com/AravisProject/aravis/archive/refs/tags/ARAVIS_0_6_4.tar.gz

tar -xvf ARAVIS_0_6_4.tar.gz

mv aravis-ARAVIS_0_6_4/ ARAVIS_0_6_4/

cd ~/SETUP_NVIDIA/ARAVIS_0_6_4

echo -e "\e[1;34mBuilding and Installing ARAVIS\e[0;39m"
meson build -Dusb=true -Dpacket-socket=true -Dviewer=true -Dgst-plugin=true -Dfast-heartbeat=true
cd build
ninja
sudo ninja install

echo export GI_TYPELIB_PATH="$GI_TYPELIB_PATH:/home/$USER/SETUP_NVIDIA/ARAVIS_0_6_4/build/src" >> ~/.bashrc
echo export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/$USER/SETUP_NVIDIA/ARAVIS_0_6_4/build/src/.libs" >> ~/.bashrc

source ~/.bashrc

sudo ldconfig

cd ~/SETUP_NVIDIA

echo -e "\e[1;34mARAVIS installed !\e[0;39m"
