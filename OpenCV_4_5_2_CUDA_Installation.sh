#!/bin/bash

echo -e "\e[1;34mPlease remove OpenCV from this computer if it's already installed !\e[0;39m"

while true; do
    read -p "Continue ? Yy or Nn " yn
    case $yn in
        [Yy]* ) echo ; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo -e "\e[1;34mYou need to have at least 8 GB of RAM available for OpenCV build !\e[0;39m"
echo -e "\e[1;34mIf you don't have enough swap, please use jtop (python3 -m pip install jetson-stats) to add some !\e[0;39m"

while true; do
    read -p "Do you have enough RAM ? Yy or Nn " yn
    case $yn in
        [Yy]* ) echo ; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo -e "\e[1;34mChecking some dependencies...\e[0;39m"

sudo sh -c "echo '/usr/local/cuda/lib64' >> /etc/ld.so.conf.d/nvidia-tegra.conf"

sudo ldconfig

sudo apt-get install build-essential cmake git unzip pkg-config\
libjpeg-dev libpng-dev libtiff-dev\
libavcodec-dev libavformat-dev libswscale-dev\
libgtk2.0-dev libcanberra-gtk*\
python3-dev python3-pip\
libxvidcore-dev libx264-dev libgtk-3-dev\
libtbb2 libtbb-dev libdc1394-22-dev\
gstreamer1.0-tools libv4l-dev v4l-utils\
libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev\
libavresample-dev libvorbis-dev libxine2-dev\
libfaac-dev libmp3lame-dev libtheora-dev\
libopencore-amrnb-dev libopencore-amrwb-dev\
libopenblas-dev libatlas-base-dev libblas-dev\
liblapack-dev libeigen3-dev gfortran\
libhdf5-dev protobuf-compiler \
libprotobuf-dev libgoogle-glog-dev libgflags-dev\

python3 -m pip install numpy


DIR="~/SETUP_NVIDIA"
if [ -d "$DIR" ]; then
  cd ~/SETUP_NVIDIA
else
  mkdir ~/SETUP_NVIDIA && cd ~/SETUP_NVIDIA
fi

mkdir opencv

cd opencv

echo -e "\e[1;34mDownloading OpenCV 4.5.2\e[0;39m"

git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git

cd opencv

git checkout 4.5.2

cd ..
cd opencv_contrib

git checkout 4.5.2

cd ../opencv

cd opencv

DIR="~/SETUP_NVIDIA/opencv/opencv/build"
if [ -d "$DIR" ]; then
  cd ~/SETUP_NVIDIA/opencv/opencv/build
else
  mkdir ~/SETUP_NVIDIA/opencv/opencv/build && cd ~/SETUP_NVIDIA/opencv/opencv/build
fi

cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D OPENCV_EXTRA_MODULES_PATH=~/SETUP_NVIDIA/opencv/opencv_contrib/modules \
-D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
-D WITH_OPENCL=OFF \
-D WITH_CUDA=ON \
-D CUDA_ARCH_BIN=5.3 \
-D CUDA_ARCH_PTX="" \
-D WITH_CUDNN=ON \
-D WITH_CUBLAS=ON \
-D ENABLE_FAST_MATH=ON \
-D CUDA_FAST_MATH=ON \
-D OPENCV_DNN_CUDA=ON \
-D ENABLE_NEON=ON \
-D WITH_QT=OFF \
-D WITH_OPENMP=ON \
-D WITH_OPENGL=ON \
-D BUILD_TIFF=ON \
-D WITH_FFMPEG=ON \
-D WITH_GSTREAMER=ON \
-D WITH_TBB=ON \
-D BUILD_TBB=ON \
-D BUILD_TESTS=OFF \
-D WITH_EIGEN=ON \
-D WITH_V4L=ON \
-D WITH_LIBV4L=ON \
-D OPENCV_ENABLE_NONFREE=ON \
-D INSTALL_C_EXAMPLES=OFF \
-D INSTALL_PYTHON_EXAMPLES=OFF \
-D BUILD_opencv_python3=TRUE \
-D OPENCV_GENERATE_PKGCONFIG=ON \
-D BUILD_EXAMPLES=OFF \
-D WITH_ARAVIS=ON \
-D BUILD_ARAVIS=ON ..

make -j$(nproc)

while true; do
    read -p "Make Install ? Yy or Nn " yn
    case $yn in
        [Yy]* ) sudo make install ; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

sudo ldconfig

make clean

sudo apt update

echo -e "\e[1;34mOpenCV 4.5.2 is ready !\e[0;39m"
echo -e "\e[1;34mPlease check using jtop that OpenCV CUDA compiled is active in INFO section and then free the swap you've just created with it !\e[0;39m"
