# Jetson Setup for GPU Computing

A repo for guidelines and procedures for setupping a Jetson (Nano here but almost the same for the others) for GPU Computing.


**I consider that your using the Ubuntu 18.04 LTS distro from NVIDIA which has already CUDA (10.2) and GPU drivers installed.**

**All these scripts will be placed in the `~/SETUP_NVIDIA` folder in your home. Please do not delete this folder as some of the builds in this folder will be linked to your system !**

## **Aravis 0.6 installation for using as OpenCV backend**

`sudo chmod +x Aravis_0_6_Installation.sh`  then `sudo ./Aravis_0_6_Installation.sh`

Do a symlink in girepository so that Aravis can be used in Python without OpenCV !

`ln -s /usr/local/lib/aarch64-linux-gnu/girepository-1.0/Aravis-0.8.typelib /usr/lib/aarch64-linux-gnu/girepository-1.0/Aravis-0.8.typelib`

If you want to use USBCameras with GStreamer you need to install SDL Lib by executing `sudo chmod +x USB_Cameras_SDL_Setup.sh` and then `./USB_Cameras_SDL_Setup.sh`

After the installation, you can test it by launching directly in CLI the command
`gst-launch-1.0 --gst-plugin-path=~/SETUP_NVIDIA/ARAVIS_0_6_4/build/gst aravissrc ! videoconvert ! autovideosink`


## **Installing OpenCV With Cuda Support On Jetson.**

`sudo chmod +x OpenCV_4_5_2_CUDA_Installation.sh`  then `./OpenCV_4_5_2_CUDA_Installation.sh`

For use of an **USBCamera** in **OpenCV** using Aravis backend, you need to create in both **Python/C++** a **VideoCapture()** empty object and then open the CAP 2100.

**Python**

```python
import cv2
cap = cv2.VideoCapture()
cap.open(2100)
```

**C++**

```cpp
cv::VideoCapture cap;
cap.open(cv::CAP_ARAVIS); //2100
if (cap.isOpened())
	cap >> frame_in;
else {
	std::cerr << "Failed to open camera." << std::endl;
	return -1;
}
```

You can also access Dim infos and set FPS

```cpp
cap.set(cv::CAP_PROP_FPS, 57.25);

unsigned int width  = cap.get(cv::CAP_PROP_FRAME_WIDTH); 
unsigned int height = cap.get(cv::CAP_PROP_FRAME_HEIGHT); 
unsigned int fps    = cap.get(cv::CAP_PROP_FPS);
unsigned int pixels = width*height;
std::cout <<"Frame size : "<<width<<" x "<<height<<", "<<pixels<<" Pixels "<<fps<<" FPS"<<std::endl;
```
