#!/bin/bash

echo -e "\e[1;34mInstallation of SDL for using USBCamera mainly with GStreamer\e[0;39m"

DIR="~/SETUP_NVIDIA"
if [ -d "$DIR" ]; then
  cd ~/SETUP_NVIDIA
else
  mkdir ~/SETUP_NVIDIA && cd ~/SETUP_NVIDIA
fi

git clone https://github.com/libsdl-org/SDL.git

cd SDL

mkdir build

cd build

../configure

make

sudo make install
